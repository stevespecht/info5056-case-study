import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { User } from './user';
import { RestfulService } from '../restful.service';
import { BASEURL } from '../constants';
@Component({
  template: `
 <mat-card>
 <mat-card-header layout="row" class="center">
      <img src="../../assets/images/wavelogo.png" height="100" width="100" />
  </mat-card-header>
 <mat-card-content>
 <app-login-detail (getuser)="login($event)"></app-login-detail>
 <span class="mat-subheading-1" style="position:absolute; right: 4vw; color: aquamarine">{{ msg }}</span>
 <br/>
 </mat-card-content>
 </mat-card>
 `
})
export class LoginHomeComponent implements OnInit {
  model: any = {};
  user: User;
  msg: string;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public restService: RestfulService
  ) { }
  ngOnInit() {
    sessionStorage.setItem('token', '');
    this.msg = 'enter login credentials';
  }
  login(user: User) {
    const url = `${BASEURL}login`;
    sessionStorage.setItem('token', user.username + ':' + user.password);
    this.restService.add(url, user)
      .subscribe(payload => {
          if (payload) { // server returns true if headers pass authentication
            this.router.navigate(['home']);
          } else {
            this.msg = 'Authentication failed.';
            sessionStorage.removeItem('token');
          }
        },
        err => {
          this.msg = 'Authentication failed.';
          sessionStorage.removeItem('token');
        });
  }
}
