import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductHomeComponent } from './product-home.component';
import { ProductDetailComponent } from './product-detail.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import {
  MatSelectModule, MatButtonModule, MatInputModule, MatToolbarModule,
  MatIconModule, MatCardModule, MatTooltipModule, MatListModule, MatTableModule,
  MatSortModule, MatExpansionModule, MatPaginatorModule, MatDialogModule
} from '@angular/material';
import { DeleteDialogComponent } from '../deletedialog/delete-dialog.component';
@NgModule({
  declarations: [ProductHomeComponent, ProductDetailComponent, DeleteDialogComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    BrowserModule,
    MatSelectModule,
    MatButtonModule,
    MatInputModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatTooltipModule,
    MatListModule,
    MatTableModule,
    MatSortModule,
    MatExpansionModule,
    MatPaginatorModule,
    MatDialogModule
  ],
  entryComponents: [DeleteDialogComponent]
})
export class ProductModule { }
