import { PurchaseItem } from './purchase-item';
/**
 * Report - interface for expense report
 */
export interface Purchase {
  id: number;
  vendorid: number;
  items: PurchaseItem[];
  amount: number;
}
