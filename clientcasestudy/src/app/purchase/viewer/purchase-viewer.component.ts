import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { Vendor } from '../../vendor/vendor';
import { RestfulService } from '../../restful.service';
// import { Product } from '../../product/product';
import { PurchaseItem } from '../purchase-item';
import { Purchase } from '../purchase';
import { BASEURL, PDFURL } from '../../constants';
import {Product} from "../../product/product";
@Component({
  templateUrl: './purchase-viewer.component.html'
})
export class PurchaseViewerComponent implements OnInit {

  generatorForm: FormGroup;
  vendorid: FormControl;
  purchaseid: FormControl;

  vendors: Array<Vendor>; // all vendors
  vendorsPurchases: Array<Purchase>; // vendors purchases
  selectedVendor: Vendor; // the current selected vendor
  selectedPurchase: Purchase; // the current selected purchase
  products: Array<Product>; // everybody's products
  items: Array<PurchaseItem>; // product items that will be in purchase
  selectedPurchaseDate: string;

  pickedPurchase: boolean;
  pickedVendor: boolean;
  hasPurchases: boolean;

  msg: string;
  subtotal: number;
  tax: number;
  total: number;
  purchaseno: number;
  url: string;

  constructor(private builder: FormBuilder, private restService: RestfulService) {
    this.pickedVendor = false;
    this.pickedPurchase = false;
    this.url = BASEURL + 'purchases';
  } // constructor
  ngOnInit() {
    this.msg = '';
    this.vendorid = new FormControl('');
    this.purchaseid = new FormControl('');
    this.generatorForm = this.builder.group({
      purchaseid: this.purchaseid,
      vendorid: this.vendorid
    });

    this.onPickVendor();
    this.onPickPurchase();
    this.msg = 'loading vendors from server...';
    this.restService.load(BASEURL + 'vendors').subscribe(
      vendorPayload => {
        this.vendors = vendorPayload._embedded.vendors;
        this.msg = 'vendors loaded';
        this.msg = 'loading  from server...';
        this.restService.load(BASEURL + 'products').subscribe(
          productPayload => {
            this.products = productPayload._embedded.products;
            this.msg = 'server data loaded';
          },
          err => {this.msg += `Error occurred - purchase orders not loaded - ${err.status} - ${err.statusText}`;
          });
      },
      err => {this.msg += ` Error occurred - vendors not loaded - ${err.status} - ${err.statusText}`;
      });
  }

  onPickVendor(): void {
    this.generatorForm.get('vendorid').valueChanges.subscribe(val => {
      this.selectedPurchase = null;
      this.selectedVendor = val;
      this.loadVendorPurchases();
      this.pickedPurchase = false;
      this.hasPurchases = false;
      this.msg = 'choose purchase order for vendor';
      this.pickedVendor = true;
      this.items = [];
    });
  }

  onPickPurchase(): void {
    this.generatorForm.get('purchaseid').valueChanges.subscribe(val => {
      this.items = [];
      this.selectedPurchase = val;
      //the viewer has no problem displaying podate when running the app
      //but for some reason my project wouldn't build because it didn't recognize podate?
      //even though it works during development
      //I fixed it by assigning podate to a separate var and that worked for some reason
      this.selectedPurchaseDate = val.podate;
      this.purchaseno = this.selectedPurchase.id;
      this.selectedPurchase.items.map(purchaseItem => { this.items.push(purchaseItem); });

      if (this.items.length > 0) {
        this.hasPurchases = true;
      }

      this.calculateTotal();
    });
  }

  loadVendorPurchases() {
    this.vendorsPurchases = [];
    this.restService.load(`${this.url}/search/findByVendor?vendorid=${this.selectedVendor.id}`).subscribe(
      purchasePayload => {
        this.vendorsPurchases = purchasePayload._embedded.purchases;
      },
      err => {this.msg += `Error occurred - purchase orders not loaded - ${err.status} - ${err.statusText}`;
      });


  } // loadVendorProducts

  calculateTotal()
  {
    this.subtotal = 0.0;
    this.tax = 0.0;
    this.total = 0.0;
    this.items.forEach(item => this.subtotal += (item.price * item.qty));
    this.tax = this.subtotal * 0.13;
    this.total =  this.subtotal + this.tax;
  }

  getProductName( item : PurchaseItem)
  {
    let product = this.products.find(product => product.id === item.productid);

    return product.name;
  }

  viewPdf() {
    window.open(PDFURL + this.purchaseno, '');
  } // viewPdf

} // PurchaseGeneratorComponent
