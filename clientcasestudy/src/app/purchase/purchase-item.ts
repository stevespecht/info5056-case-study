/**
 * PurchaseItem - container class for product purchase item
 */
export interface PurchaseItem {
  id: number;
  purchaseid: number;
  productid: string;
  qty: number;
  price: number;
}
