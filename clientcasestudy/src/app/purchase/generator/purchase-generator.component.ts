import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder } from '@angular/forms';
import { Vendor } from '../../vendor/vendor';
import { RestfulService } from '../../restful.service';
import { Product } from '../../product/product';
import { PurchaseItem } from '../purchase-item';
import { Purchase } from '../purchase';
import { BASEURL, PDFURL } from '../../constants';
@Component({
  templateUrl: './purchase-generator.component.html'
})
export class PurchaseGeneratorComponent implements OnInit {
  // form
  generatorForm: FormGroup;
  vendorid: FormControl;
  productid: FormControl;
  qtyid: FormControl;
  // component
  products: Array<Product>; // everybody's products
  vendors: Array<Vendor>; // all vendors
  items: Array<PurchaseItem>; // product items that will be in purchase
  selectedproducts: Array<Product>; // products that being currently chosen
  vendorproducts: Array<Product>; // all products for a particular vendor
  quantities: Array<String>; // all products for a particular vendor
  selectedQty: string; // the current selected product
  selectedProduct: Product; // the current selected product
  selectedVendor: Vendor; // the current selected vendor
  pickedProduct: boolean;
  pickedVendor: boolean;
  generated: boolean;
  hasProducts: boolean;
  msg: string;
  subtotal: number;
  tax: number;
  total: number;
  purchaseno: number;
  url: string;
  purchaseItemFound : PurchaseItem;
  constructor(private builder: FormBuilder, private restService: RestfulService) {
    this.pickedVendor = false;
    this.pickedProduct = false;
    this.generated = false;
    this.url = BASEURL + 'api/purchases';
  } // constructor
  ngOnInit() {
    //this.
    this.msg = '';
    this.vendorid = new FormControl('');
    this.productid = new FormControl('');
    this.qtyid = new FormControl('');
    this.generatorForm = this.builder.group({
      productid: this.productid,
      vendorid: this.vendorid,
      qtyid: this.qtyid
    });
    this.qtyid.setValue("EOQ", {emitEvent: false});
    this.onPickVendor();
    this.onPickProduct();
    this.onPickQty();

    this.msg = 'loading vendors from server...';
    this.restService.load(BASEURL + 'vendors').subscribe(
      vendorPayload => {
        this.vendors = vendorPayload._embedded.vendors;
        this.msg = 'vendors loaded';
        this.msg = 'loading products from server...';
        this.restService.load(BASEURL + 'products').subscribe(
          productPayload => {
            this.products = productPayload._embedded.products;
            this.msg = 'server data loaded';
          },
          err => {this.msg += `Error occurred - products not loaded - ${err.status} - ${err.statusText}`;
          });
      },
      err => {this.msg += ` Error occurred - vendors not loaded - ${err.status} - ${err.statusText}`;
      });
  } // ngOnInit
  /**
   * onPickVendor - subscribe to the select change event then load specific
   * vendor products for subsequent selection
   */
  onPickVendor(): void {
    this.generatorForm.get('vendorid').valueChanges.subscribe(val => {
      this.selectedProduct = null;
      this.selectedQty = null;
      this.selectedVendor = val;
      this.loadVendorProducts();
      this.pickedProduct = false;
      this.hasProducts = false;
      this.msg = 'choose product for vendor';
      this.pickedVendor = true;
      this.generated = false;
      this.items = [];
      this.selectedproducts = [];
    });
  }
  /**
   * onPickProduct - subscribe to the select change event then
   * update array containing items.
   */
  onPickProduct(): void {
    this.generatorForm.get('productid').valueChanges.subscribe(val => {
      this.selectedProduct = val;
      this.pickedProduct = true;
      this.hasProducts = true;

      this.purchaseItemFound = null;
      this.purchaseItemFound = this.items.find(it => it.productid === this.selectedProduct.id);
      if (this.purchaseItemFound != null) { // ignore entry
        if(this.purchaseItemFound.qty == this.selectedProduct.eoq)
        {
          this.qtyid.setValue("EOQ", {emitEvent:false});
        }
        else
        {
          this.qtyid.setValue(this.purchaseItemFound.qty.toString(), {emitEvent:false});
        }
      }
      else {
          this.qtyid.setValue("EOQ", {emitEvent:false});
          this.addProduct(this.selectedProduct.eoq);
        }
    });
  }

  onPickQty(): void {
    this.generatorForm.get('qtyid').valueChanges.subscribe(val => {
      this.selectedQty = val;
      //remove selected product from purchase
      if (this.selectedQty === "0") {
        this.items = this.items.filter(item => item.productid !== this.selectedProduct.id);
        this.selectedproducts = this.selectedproducts.filter(sProd => sProd.id !== this.selectedProduct.id);
        this.calculateTotal();
        this.msg = "No Products Selected";
      }
      else if(this.selectedQty === "EOQ")
      {
        this.addProduct(this.selectedProduct.eoq);
      }
      // add
      else {
        this.addProduct(parseInt(this.selectedQty));
      }
      if (this.items.length > 0) {
        this.hasProducts = true;
      }
      else {this.hasProducts = false;}
    });
  }

  addProduct(qty)
  {
    //update
    if (this.items.find(it => it.productid === this.selectedProduct.id)) { // ignore entry
      let item = this.items.findIndex(item => item.productid == this.selectedProduct.id)
      if(item !== -1)
      {
        this.items[item] = {id: 0, purchaseid: 0, productid: this.selectedProduct.id, qty : qty, price : this.selectedProduct.costprice};
      }

      let sProd = this.selectedproducts.findIndex(sProd => sProd.id === this.selectedProduct.id)
      if(sProd !== -1)
      {
        this.selectedproducts[sProd] = this.selectedProduct;
      }
    }
    //add
    else {
      const item: PurchaseItem = {id: 0, purchaseid: 0, productid: this.selectedProduct.id, qty : qty, price : this.selectedProduct.costprice};
      this.items.push(item);
      this.selectedproducts.push(this.selectedProduct);
    }

    this.calculateTotal();
    this.msg = qty + " " + this.selectedProduct.name + "(s) Added";
  }

  calculateTotal()
  {
    this.subtotal = 0.0;
    this.tax = 0.0;
    this.total = 0.0;
    this.items.forEach(item => this.subtotal += (item.price * item.qty));
    this.tax = this.subtotal * 0.13;
    this.total =  this.subtotal + this.tax;
  }

  /**
   * loadVendorProducts - filter for a particular vendor's products
   */
  loadVendorProducts() {
    this.vendorproducts = [];
    this.vendorproducts = this.products.filter(ex => ex.vendorid === this.selectedVendor.id); // filter products for single vendor
  } // loadVendorProducts
  /**
   * createPurchase - create the client side purchase
   */
  createPurchase() {
    this.generated = false;
    const purchase: Purchase =
      {id: 0, items: this.items, vendorid: this.selectedProduct.vendorid, amount: this.total};
    this.restService.add(this.url, purchase).subscribe(
      purchaseId => {
        if (purchaseId > 0) { // server returns new purchase#
          this.msg = `Product Purchase ${purchaseId} created!`;
          this.generated = true;
          this.purchaseno = purchaseId;
        } else {
          this.msg = 'Purchase not created! - server error';
        }
        this.hasProducts = false;
        this.pickedVendor = false;
        this.pickedProduct = false;
      },
      err => {
        this.msg = `Error occurred - purchase not created - ${err.status} - ${err.statusText}`;
      }
    );
  } // createPurchase

  /**
   * viewPdf - determine report number and pass to server
   * for PDF generation in a new window
   */
  viewPdf() {
    window.open(PDFURL + this.purchaseno, '');
  } // viewPdf

} // PurchaseGeneratorComponent
