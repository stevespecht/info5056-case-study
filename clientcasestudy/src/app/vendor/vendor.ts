export interface Vendor {
  /**
   * Vendor - interface for a vendor
   */
  id: number;
  name : string;
  address1 : string;
  city : string;
  province : string;
  postalcode : string;
  phone : string;
  type : string;
  email : string;
}
