package com.info5059.casestudy.product;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import javax.persistence.*;
import java.math.BigDecimal;
/**
 * Expense entity
 */
@Entity
@Data
@RequiredArgsConstructor
public class Product {
    @Id
    private String Id;
    private int vendorid;
    private String name;
    private BigDecimal costprice;
    private BigDecimal msrp;
    private int rop;
    private int eoq;
    private int qoh;
    private int qoo;
    // needed in case 2
    @Basic(optional = true)
    @Lob
    private byte[] qrcode;
    private String qrcodetxt;
}
