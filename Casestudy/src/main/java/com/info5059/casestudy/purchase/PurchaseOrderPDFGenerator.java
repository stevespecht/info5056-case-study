package com.info5059.casestudy.purchase;

import com.info5059.casestudy.product.Product;
import com.info5059.casestudy.product.ProductRepository;
import com.info5059.casestudy.vendor.Vendor;
import com.info5059.casestudy.vendor.VendorRepository;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.web.servlet.view.document.AbstractPdfView;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.net.URL;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * Generator - a class for testing how to create dynamic output in PDF
 * format using the iText library
 */
public abstract class PurchaseOrderPDFGenerator extends AbstractPdfView {
    public static ByteArrayInputStream generatePurchaseOrder(String poid,
                                                      PurchaseOrderDAO repDAO,
                                                      VendorRepository vendorRepository,
                                                      ProductRepository productRepository) {
        URL imageUrl = com.info5059.casestudy.purchase.PurchaseOrderPDFGenerator.class.getResource("/public/images/wavelogo.png");
        Document document = new Document();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Font catFont = new Font(Font.FontFamily.HELVETICA, 24, Font.BOLD);
        Font subFont = new Font(Font.FontFamily.HELVETICA, 16, Font.BOLD);
        Font smallBold = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);
        Locale locale = new Locale("en", "US");
        NumberFormat formatter = NumberFormat.getCurrencyInstance(locale);

        try {
            PurchaseOrder purchaseOrder = repDAO.findOne(Long.parseLong(poid));
            PdfWriter.getInstance(document, baos);
            document.open();
            Paragraph preface = new Paragraph();
            // add the logo here
            Image image1 = Image.getInstance(imageUrl);
            image1.scalePercent(30f);
            image1.setAbsolutePosition(55f, 725f);
            preface.add(image1);
            preface.setAlignment(Element.ALIGN_RIGHT);
            // Lets write a big header
            Paragraph mainHead = new Paragraph(String.format("%55s", "PURCHASE ORDER"), catFont);
            preface.add(mainHead);
            preface.add(new Paragraph(String.format("%90s", "PO#:" + poid), subFont));
            addEmptyLine(preface, 3);

            //Vendor info table
            Optional<Vendor> opt = vendorRepository.findById(purchaseOrder.getVendorid());
            if (opt.isPresent()) {

                Vendor vendor = opt.get();

                PdfPTable vendorTable = new PdfPTable(2);
                vendorTable.setWidthPercentage(40);
                vendorTable.setHorizontalAlignment(Element.ALIGN_LEFT);

                PdfPCell cell = new PdfPCell(new Paragraph("Vendor:", smallBold));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell.setBorder(0);
                vendorTable.addCell(cell);

                //Name
                cell = new PdfPCell(new Paragraph(vendor.getName()));
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
                cell.setBorder(0);
                vendorTable.addCell(cell);

                cell = new PdfPCell(new Paragraph(""));
                cell.setBorder(0);
                vendorTable.addCell(cell);

                //Address
                cell = new PdfPCell(new Paragraph(vendor.getAddress1()));
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
                cell.setBorder(0);
                vendorTable.addCell(cell);

                cell = new PdfPCell(new Paragraph(""));
                cell.setBorder(0);
                vendorTable.addCell(cell);

                //City
                cell = new PdfPCell(new Paragraph(vendor.getCity()));
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
                cell.setBorder(0);
                vendorTable.addCell(cell);

                cell = new PdfPCell(new Paragraph(""));
                cell.setBorder(0);
                vendorTable.addCell(cell);

                //Province
                cell = new PdfPCell(new Paragraph(vendor.getProvince()));
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
                cell.setBorder(0);
                vendorTable.addCell(cell);

                cell = new PdfPCell(new Paragraph(""));
                cell.setBorder(0);
                vendorTable.addCell(cell);

                //Postal code
                cell = new PdfPCell(new Paragraph(vendor.getPostalcode()));
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
                cell.setBorder(0);
                vendorTable.addCell(cell);

                preface.add(vendorTable);
            }

            addEmptyLine(preface, 1);

            // 3 column table
            PdfPTable table = new PdfPTable(5);
            PdfPCell cell = new PdfPCell(new Paragraph("Product Code"));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("Product Description"));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("Quantity Sold"));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("Price"));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);
            cell = new PdfPCell(new Paragraph("Ext Price"));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);

            BigDecimal subtotal = new BigDecimal(0.0);
            BigDecimal tax = new BigDecimal(0.0);
            BigDecimal total = new BigDecimal(0.0);
            // dump out the line items
            for (PurchaseOrderLineitem line : purchaseOrder.getItems()) {

                Optional<Product> optx = productRepository.findById(line.getProductid());

                if (optx.isPresent()) {
                    Product product = optx.get();

                    cell = new PdfPCell(new Phrase(product.getId().toString()));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(product.getName()));
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(Integer.toString(line.getQty())));
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(formatter.format(line.getPrice())));
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(formatter.format(line.getPrice().multiply(new BigDecimal(line.getQty())))));
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    table.addCell(cell);

                    subtotal = subtotal.add(line.getPrice().multiply(new BigDecimal(line.getQty())), new MathContext(8, RoundingMode.UP));
                }
            }

            tax = subtotal.multiply(new BigDecimal(0.13));
            total = subtotal.add(tax);

            // purchaseOrder total
            cell = new PdfPCell(new Phrase(""));
            cell.setBorder(0);
            cell.setColspan(3);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Total:"));
            cell.setBorder(0);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(formatter.format(subtotal)));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(""));
            cell.setBorder(0);
            cell.setColspan(3);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Tax:"));
            cell.setBorder(0);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(formatter.format(tax)));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(""));
            cell.setBorder(0);
            cell.setColspan(3);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Order Total:"));
            cell.setBorder(0);
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(formatter.format(total)));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBackgroundColor(BaseColor.YELLOW);
            table.addCell(cell);

            preface.add(table);

            addEmptyLine(preface, 3);
            preface.setAlignment(Element.ALIGN_CENTER);
            SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            preface.add(new Paragraph(String.format("%85s", "PO Generated on: " + formatDate.format(new Date()))));
            document.add(preface);
            document.close();
        } catch (Exception ex) {
            Logger.getLogger(PurchaseOrderPDFGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new ByteArrayInputStream(baos.toByteArray());
    }
    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }
}
