package com.info5059.casestudy.purchase;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@Repository
//@CrossOrigin
@RepositoryRestResource(collectionResourceRel = "purchases", path = "purchases")
public interface PurchaseRepository extends CrudRepository<PurchaseOrder, Long> {

    @Query("select p from PurchaseOrder p where p.vendorid = ?1")
    List<PurchaseOrder> findByVendor(Long vendorid);

}