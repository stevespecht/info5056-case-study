INSERT INTO Vendor (Address1,City,Province,PostalCode,Phone,Type,Name,Email)
 VALUES ('123 Maple St','London','On', 'N1N-1N1','(555)555-5555','Trusted','ABC Supply Co.','abc@supply.com');
INSERT INTO Vendor (Address1,City,Province,PostalCode,Phone,Type,Name,Email)
 VALUES ('543 Sycamore Ave','Toronto','On', 'N1P-1N1','(999)555-5555','Trusted','Big Bills Depot','bb@depot.com');
INSERT INTO Vendor (Address1,City,Province,PostalCode,Phone,Type,Name,Email)
 VALUES ('922 Oak St','London','On', 'N1N-1N1','(555)555-5599','Un Trusted','Shady Sams','ss@underthetable.com');
INSERT INTO Vendor (Address1,City,Province,PostalCode,Phone,Type,Name,Email)
 VALUES ('300 Main St','London','On', 'N9A-4B8','(555)555-0000','Un Trusted','Steven Specht','specht@store.com');
-- add some expenses to seed the table
INSERT INTO Product (Id,VendorId,Name,CostPrice,MSRP,ROP,EOQ,QOH,QOO)
VALUES ('12X45',1,'Paddleboard',329.98,359.99,5,5,10,0);
INSERT INTO Product (Id,VendorId,Name,CostPrice,MSRP,ROP,EOQ,QOH,QOO)
VALUES ('14X45',1,'Board Wax',19.98,29.99,50,50,100,0);
INSERT INTO Product (Id,VendorId,Name,CostPrice,MSRP,ROP,EOQ,QOH,QOO)
VALUES ('16X45',1,'Surfboard',729.98,759.99,15,15,10,0);
INSERT INTO Product (Id,VendorId,Name,CostPrice,MSRP,ROP,EOQ,QOH,QOO)
VALUES ('18X45',1,'Sailboard',629.98,659.99,3,3,5,0);
INSERT INTO Product (Id,VendorId,Name,CostPrice,MSRP,ROP,EOQ,QOH,QOO)
VALUES ('12Y45',2,'Paddleboard',129.98,159.99,5,10,20,0);
INSERT INTO Product (Id,VendorId,Name,CostPrice,MSRP,ROP,EOQ,QOH,QOO)
VALUES ('14Y45',2,'Board Wax',19.98,29.99,50,50,100,0);
INSERT INTO Product (Id,VendorId,Name,CostPrice,MSRP,ROP,EOQ,QOH,QOO)
VALUES ('16Y45',2,'Surfboard',529.98,559.99,2,2,6,0);
INSERT INTO Product (Id,VendorId,Name,CostPrice,MSRP,ROP,EOQ,QOH,QOO)
VALUES ('12Z45',3,'Paddleboard',129.98,159.99,5,10,20,0);
INSERT INTO Product (Id,VendorId,Name,CostPrice,MSRP,ROP,EOQ,QOH,QOO)
VALUES ('14Z45',3,'Board Wax',19.98,29.99,50,50,100,0);
INSERT INTO Product (Id,VendorId,Name,CostPrice,MSRP,ROP,EOQ,QOH,QOO)
VALUES ('16Z45',3,'Surfboard',529.98,559.99,2,2,6,0);
INSERT INTO Product (Id,VendorId,Name,CostPrice,MSRP,ROP,EOQ,QOH,QOO)
VALUES ('12S45',4,'Paddleboard',329.98,359.99,5,5,10,0);
INSERT INTO Product (Id,VendorId,Name,CostPrice,MSRP,ROP,EOQ,QOH,QOO)
VALUES ('14S45',4,'Board Wax',19.98,29.99,50,50,100,0);
INSERT INTO Product (Id,VendorId,Name,CostPrice,MSRP,ROP,EOQ,QOH,QOO)
VALUES ('16S45',4,'Surfboard',729.98,759.99,15,15,10,0);
INSERT INTO Product (Id,VendorId,Name,CostPrice,MSRP,ROP,EOQ,QOH,QOO)
VALUES ('18S45',4,'Sailboard',629.98,659.99,3,3,5,0);

